export const ADD = "ADD";
export const UPDATE = "UPDATE";
export const DELETE = "DELETE";
export const FETCH_DATA = "FETCH_DATA";
export const RECIEVE_DATA = "RECIEVE_DATA";
export const CHANGE_USER = "CHANGE_USER";
export const ADD_POST_TO_STATE = "ADD_POST_TO_STATE";
