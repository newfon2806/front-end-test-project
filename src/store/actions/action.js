import axios from "axios";

import * as actionTypes from "./actionType";
import env from "../../config";

// const url = "http://localhost:5555/cards";

export const addPost = (value) => {
  return (dispatch) => {
    axios
      .post(env.API_URL, value)
      .then((response) => {
        dispatch(addPostToState(response.data.data));
        //console.log(response);
      })
      .catch((error) => {
        console.log("post error: " + error);
      });
  };
};

export const addPostToState = (data) => {
  return {
    type: actionTypes.ADD_POST_TO_STATE,
    data: data,
  };
};

export const updatePost = (id, value) => {
  return (dispatch) => {
    const temp_url = env.API_URL + "/" + id;
    axios
      .patch(temp_url, value)
      .then((response) => {
        dispatch(fetchData());
      })
      .catch((error) => {
        console.log("patch error: " + error);
      });
  };
};

export const removePost = (id) => {
  return (dispatch) => {
    const temp_url = env.API_URL + "/" + id;
    axios
      .delete(temp_url)
      .then((response) => {
        dispatch(fetchData());
      })
      .catch((error) => {
        console.log("delete error: " + error);
      });
  };
};

export const fetchData = () => {
  return (dispatch) => {
    axios
      .get(env.API_URL)
      .then((response) => {
        dispatch(recieveData(response.data.data));
      })
      .catch((error) => {
        console.log("fetchData error: " + error);
      });
  };
};

export const recieveData = (data) => {
  return {
    type: actionTypes.RECIEVE_DATA,
    data: data,
  };
};

export const chanegUser = (value) => {
  return {
    type: actionTypes.CHANGE_USER,
    value: value,
  };
};
