import * as actionTypes from "../actions/actionType";
import { updateObject } from "../utility";

const initialState = {
  posts: [],
  current_user: "Daniel Ortiz",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD:
      return state;
    case actionTypes.UPDATE:
      return state;
    case actionTypes.DELETE:
      return state;
    case actionTypes.RECIEVE_DATA:
      return updateObject(state, { posts: action.data });
    case actionTypes.CHANGE_USER:
      return updateObject(state, { current_user: action.value });
    case actionTypes.ADD_POST_TO_STATE:
      return updateObject(state, { posts: state.posts.concat([action.data]) });
    default:
      return state;
  }
};

export default reducer;
