import React from "react";
import "./App.css";

import Dashboard from "./containers/Dashbaord/Dashboard";
import NavBar from "./components/NavBar/NavBar";

function App() {
  return (
    <div className="App">
      <NavBar />
      <Dashboard />
    </div>
  );
}

export default App;
