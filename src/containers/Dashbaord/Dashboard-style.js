import styled from "styled-components";

const DashboardStyle = styled.div`
  column-count: 4;
  column-gap: 15px;
  padding-top: 65px;
`;

export default DashboardStyle;
