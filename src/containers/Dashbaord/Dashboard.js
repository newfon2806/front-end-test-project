import React, { Component } from "react";
import DashboardStyle from "./Dashboard-style";

import Cards from "../../components/Cards/Cards";

class Dashboard extends Component {

  render() {
    return (
      <DashboardStyle className="grid">
        <Cards />
      </DashboardStyle>
    );
  }
}

export default Dashboard;
