import React, { Component } from "react";

import CardStyled from "./Card-style";
import EditButton from "../../Button/Edit/Edit";
import DeleteButton from "../../Button/Delete/Delete";
import "../../../assets/iconfont/style.css";

class Card extends Component {
  render() {
    return (
      <CardStyled className="card">
        <div className="headerwrap">
          <div className="title">{this.props.post.title}</div>
          <div>
            <EditButton post={this.props.post}></EditButton>
            <DeleteButton post={this.props.post}></DeleteButton>
          </div>
        </div>
        <div>{this.props.post.message}</div>
        <div className="itemswrap">
          <div>
            <span className="icon-heart"></span>
            {this.props.post.like_count}
          </div>
          <div>
            {this.props.post.comment_count}{" "}
            {this.props.post.comment_count > 1 ? "Comments" : "Comment"}
          </div>
        </div>
        <div>
          {this.props.post.author.first_name +
            " " +
            this.props.post.author.last_name}
        </div>
      </CardStyled>
    );
  }
}

export default Card;
