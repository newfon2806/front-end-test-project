import styled from "styled-components";

const CardStyled = styled.div`
  background-color: #ffffff;
  padding: 15px;
  display: inline-block;
  margin-bottom: 15px;
  width: 100%;
  box-sizing: border-box;

  > div:not(:last-child) {
    margin-bottom: 15px;
  }

  .headerwrap {
    display: flex;
    justify-content: space-between;
    align-items: center;

    > .title {
      color: #9185b7;
      font-size: 16px;
      font-weight: 600;
    }
  }

  .itemswrap {
    display: flex;
    align-items: center;

    span {
      margin-right: 5px;
      font-size: 16px;
    }

    div:not(:last-child) {
      border-right: 1px solid #b3b3b8;
    }
    div:first-child {
      padding-right: 10px;
    }
    div:not(:first-child) {
      margin-left: 10px;
    }
  }
`;

export default CardStyled;
