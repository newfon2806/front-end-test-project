import React, { Component } from "react";
import { connect } from "react-redux";

import Card from "./Card/Card";
import * as actionCreators from "../../store/actions/action";

class Cards extends Component {
  componentDidMount() {
    this.props.onFetchPost();
  }
  render() {
    return (
      <>
        {this.props.posts.map((post) => {
          return <Card key={post.id} post={post} />;
        })}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFetchPost: () => dispatch(actionCreators.fetchData()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
