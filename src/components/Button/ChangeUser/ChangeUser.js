import React, { Component } from "react";
import { connect } from "react-redux";

import * as actionCreators from "../../../store/actions/action";
import ButtonStyled from "../Button-style";
import Modal from "../../UI/Modal/Modal";

class ChangeUserButton extends Component {
  constructor(props) {
    super(props);
    this.user_ref = React.createRef();
  }

  state = {
    showModal: false,
  };
  changeUserButtonHandler = () => {
    this.setState({ showModal: true });
  };
  cancelButtonHandler = () => {
    this.setState({ showModal: false });
  };
  handleSubmit = (event) => {
    event.preventDefault();

    this.props.onChangeUser(this.user_ref.current.value);
    this.cancelButtonHandler();
  };
  render() {
    return (
      <>
        <ButtonStyled onClick={this.changeUserButtonHandler}>
          <span>Change User</span>
        </ButtonStyled>
        <Modal
          showModal={this.state.showModal}
          closeModal={this.cancelButtonHandler}
          title="Change User"
        >
          <div style={{ marginBottom: "15px" }}>
            Current User: {this.props.user}
          </div>
          <form onSubmit={this.handleSubmit}>
            <div className="input-label">Please type new user</div>
            <input type="text" ref={this.user_ref}></input>
            <ButtonStyled type="submit">
              <span>Confirm</span>
            </ButtonStyled>
          </form>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    posts: state.posts,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onChangeUser: (value) => dispatch(actionCreators.chanegUser(value)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangeUserButton);
