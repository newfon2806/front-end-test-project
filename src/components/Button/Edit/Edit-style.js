import styled from "styled-components";

const EditButtonStyled = styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;

  :disabled {
    cursor: unset;
  }

  :focus,
  :active {
    outline: none;
  }

  span {
    color: #23b4e5;
    font-size: 16px;
  }
`;

export default EditButtonStyled;
