import React, { Component } from "react";
import { connect } from "react-redux";

import "../../../assets/iconfont/style.css";
import EditButtonStyled from "./Edit-style";
import Modal from "../../UI/Modal/Modal";
import Form from "../../Form/Form";

class EditButton extends Component {
  state = {
    showModal: false,
  };
  editButtonHandler = () => {
    this.setState({ showModal: true });
  };
  cancelButtonHandler = () => {
    this.setState({ showModal: false });
  };
  render() {
    return (
      <>
        <EditButtonStyled
          onClick={this.editButtonHandler}
          disabled={
            this.props.post.author.first_name +
              " " +
              this.props.post.author.last_name !==
            this.props.user
          }
        >
          <span className="icon-pencil"> </span>{" "}
        </EditButtonStyled>{" "}
        <Modal
          showModal={this.state.showModal}
          closeModal={this.cancelButtonHandler}
          title="Edit Post"
        >
          <Form
            request="patch"
            post={this.props.post}
            user={this.props.user}
            close={this.cancelButtonHandler}
          ></Form>{" "}
        </Modal>{" "}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.current_user,
  };
};

export default connect(mapStateToProps, null)(EditButton);
