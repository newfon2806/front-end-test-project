import React, { Component } from "react";
import { connect } from "react-redux";

import ButtonStyled from "../Button-style";
import Modal from "../../UI/Modal/Modal";
import Form from "../../Form/Form";

class NewPostButton extends Component {
  state = {
    showModal: false,
  };
  newPostButtonHandler = () => {
    this.setState({ showModal: true });
  };
  cancelButtonHandler = () => {
    this.setState({ showModal: false });
  };
  render() {
    return (
      <>
        <ButtonStyled onClick={this.newPostButtonHandler}>
          <span>Add New Post</span>
        </ButtonStyled>
        <Modal
          showModal={this.state.showModal}
          closeModal={this.cancelButtonHandler}
          title="New Post"
        >
          <Form
            request="post"
            user={this.props.user}
            post={this.props.post}
            close={this.cancelButtonHandler}
          ></Form>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.current_user,
  };
};

export default connect(mapStateToProps, null)(NewPostButton);
