import styled from "styled-components";

const DeleteButtonStyled = styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;

  :disabled {
    cursor: unset;
  }

  :focus,
  :active {
    outline: none;
  }

  span {
    color: #a86f85;
    font-size: 16px;
  }
`;

export default DeleteButtonStyled;
