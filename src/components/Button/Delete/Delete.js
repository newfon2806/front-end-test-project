import React, { Component } from "react";
import { connect } from "react-redux";

import * as actionCreators from "../../../store/actions/action";
import "../../../assets/iconfont/style.css";
import DeleteButtonStyled from "./Delete-style";

class DeleteButton extends Component {
  deleteButtonHandler = () => {
    this.props.onDeletePost(this.props.post.id);
  };
  render() {
    return (
      <DeleteButtonStyled
        disabled={
          this.props.post.author.first_name +
            " " +
            this.props.post.author.last_name !==
          this.props.user
        }
        onClick={this.deleteButtonHandler}
      >
        <span className="icon-bin"></span>
      </DeleteButtonStyled>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.current_user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDeletePost: (id) => dispatch(actionCreators.removePost(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DeleteButton);
