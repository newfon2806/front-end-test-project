import React, { Component } from "react";
import { connect } from "react-redux";

import NavBarStyled from "./NavBar-style";
import NewPostButton from "../Button/NewPost/NewPost";
import ChangeUserButton from "../Button/ChangeUser/ChangeUser";

class NavBar extends Component {
  render() {
    return (
      <NavBarStyled>
        <div className="header-text">Hi, {this.props.user} !</div>
        <div style={{ display: "flex" }}>
          <ChangeUserButton user={this.props.user}></ChangeUserButton>
          <NewPostButton></NewPostButton>
        </div>
      </NavBarStyled>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.current_user,
  };
};

export default connect(mapStateToProps)(NavBar);
