import styled from "styled-components";

const NavBarStyled = styled.div`
  height: 65px;
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background-color: #ffffff;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 20px;
  box-sizing: border-box;
  z-index: 90;

  .header-text {
    color: #26b8de;
    font-size: 20px;
    font-weight: 600;
  }
`;

export default NavBarStyled;
