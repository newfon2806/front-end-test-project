import React, { Component } from "react";
import { connect } from "react-redux";

import * as actionCreators from "../../store/actions/action";
import ButtonStyled from "../Button/Button-style";

class Form extends Component {
  constructor(props) {
    super(props);
    this.title_ref = React.createRef();
    this.message_ref = React.createRef();
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const name = this.props.user.split(" ");
    const data = {
      title: this.title_ref.current.value,
      message: this.message_ref.current.value,
      like_count: 0,
      comment_count: 0,
      author: {
        first_name: name[0],
        last_name: name[1],
      },
    };
    switch (this.props.request) {
      case "patch":
        data.like_count = this.props.post.like_count;
        data.comment_count = this.props.comment_count;
        this.props.onUpdatePost(this.props.post.id, data);
        this.props.close();
        break;
      case "post":
        this.props.onAddPost(data);
        this.props.close();
        break;
      default:
        break;
    }
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div>
          <div className="input-label">Title: </div>
          <input type="text" ref={this.title_ref}></input>
        </div>
        <div>
          <div className="input-label">Message: </div>
          <textarea type="text" ref={this.message_ref}></textarea>
        </div>
        <ButtonStyled type="submit">
          <span>Submit</span>
        </ButtonStyled>
      </form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onUpdatePost: (id, value) => dispatch(actionCreators.updatePost(id, value)),
    onAddPost: (value) => dispatch(actionCreators.addPost(value)),
  };
};

export default connect(null, mapDispatchToProps)(Form);
