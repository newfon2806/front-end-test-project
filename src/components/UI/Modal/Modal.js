import React, { Component } from "react";

import ModalStyled from "./Modal-style";

class Modal extends Component {
  render() {
    return (
      <ModalStyled className={this.props.showModal === true ? "display" : ""}>
        <div className="title">{this.props.title}</div>
        <button className="close" onClick={this.props.closeModal}>
          x
        </button>
        {this.props.children}
      </ModalStyled>
    );
  }
}

export default Modal;
