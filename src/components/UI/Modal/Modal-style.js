import styled from "styled-components";

const ModalStyled = styled.div`
  visibility: hidden;
  opacity: 0;
  position: fixed;
  z-index: 500;
  border: none;
  background-color: white;
  width: 50%;
  height: 35%;
  max-width: 450px;
  box-shadow: 0 12px 15px 0 rgba(0, 0 , 0 , 0.25);
  padding: 30px 50px;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  box-sizing: border-box;
  transition: all 0.3s ease-out;

  &.display {
    visibility: visible;
    opacity: 1;
  }

  .close {
      position: absolute;
      top: 10px;
      right: 10px;
      background-color: transparent;
      border: none;
      cursor: pointer;

      &:focus, :active {
        outline: none;
      }
  }

  .title {
      font-size: 25px;
      color: #26b8de;
      margin-bottom: 20px;
  }

  input {
      margin-bottom: 10px;
  }
`;

export default ModalStyled;
