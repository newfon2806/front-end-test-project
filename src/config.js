const dev = {
  API_URL: "http://localhost:5555/cards",
};

const prod = {
  API_URL: "http://localhost:5555/cards",
};

const config = process.env.REACT_APP_STAGE === "production" ? prod : dev;

export default config;
